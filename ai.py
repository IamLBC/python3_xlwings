# author:bingcheng li
# date:2021/12/01 14:03
# email:740194688@qq.com
# coding=uft-8

import itchat
import requests


def get_reply(keyword):
    try:
        url = f"https://open.drea.cc/bbsapi/chat/get?keyWord={keyword}&userName=type%3Dbbs"
        res = requests.get(url)
        data = res.json()
        return data['data']['reply']
    except Exception as e:
        return "ops, 我还很笨，不造你在说啥"


@itchat.msg_register(itchat.content.TEXT, isFriendChat=True)
def auto_reply(msg):
    reply = "excuse me?"
    try:
        reply = get_reply(msg.text)
    except:
        pass
    finally:
        print(f'[In] {msg.text} \t [Out] {reply}')
        return reply

def main():
    itchat.auto_login(hotReload=True)
    itchat.run()


if __name__ == '__main__':
    main()

