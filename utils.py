# author:bingcheng li
# date:2021/12/01 13:56
# email:740194688@qq.com
# coding=uft-8

import time, json


def ask(menus, ques):
    print()
    for index, value in enumerate(menus):
        print(f'{index}：{value}')
    print('----------------------------------------------------')
    return int(input(ques + '：'))


def log(msg):
    print(f'log_msg: {msg}')
    with open('./log.txt', 'a', encoding='utf-8') as log_file:
        log_file.write(cur_time() + ' | ' + msg + '\n')


def cur_time():
    ocaltime = time.asctime(time.localtime(time.time()))
    cur = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    return cur


def print_json(data):
    print(json.dumps(data, sort_keys=True, indent=4, separators=(', ', ': '), ensure_ascii=False))


if __name__ == '__main__':
    cur_time()


