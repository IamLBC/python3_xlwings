# author:bingcheng li
# date:2021/12/01 14:03
# email:740194688@qq.com
# coding=uft-8

import os, utils
from PIL import Image

img_input = ''
img_out = ''


def get_size(file):
    # 获取文件大小:KB
    size = os.path.getsize(file)
    return int(size // 1024)


def resize_img(file, ratio=0.9, mb=10, quality=95):
    abp = os.path.join(img_input, file)
    raw_size = get_size(abp)
    save_img_path = os.path.join(img_out, file)

    im = Image.open(abp)
    x, y = im.size

    if raw_size <= mb:
        Image.open(abp).save(save_img_path, quality=quality)
        print('%s Original image size <= mb: %s x %s %sKB' % (file, x, y, get_size(abp)))
    else:
        print('%s Original image size: %s x %s %sKB' % (file, x, y, get_size(abp)))
        while raw_size > mb:
            im = Image.open(abp)
            x, y = im.size

            # resize
            im.resize((int(x * ratio), int(y * ratio)), Image.ANTIALIAS).save(save_img_path, quality=quality)

            # thumbnail
            # im.thumbnail((width * ratio, height * ratio))
            # im.save(save_img_path, quality=quality)

            if raw_size > mb:
                raw_size = get_size(save_img_path)
                abp = save_img_path
                continue

        print('out: %sKB' % raw_size)


def start(_type, val):
    files = os.listdir(img_input)
    for file in files:
        name, ext = os.path.splitext(file)
        ext = ext.lower()
        if ext == '.jpg' or ext == '.png' or ext == '.jpeg':
            size = get_size(os.path.join(img_input, file))
            mb = val if _type == 'fixed' else int(val / 100 * size)
            resize_img(file, mb=mb)


def main():
    p = os.getcwd()
    global img_input
    global img_out
    img_input = os.path.join(p, 'img_input')
    img_out = os.path.join(p, 'img_out')
    if not os.path.exists(img_input):
        os.mkdir(img_input)
    if not os.path.exists(img_out):
        os.mkdir(img_out)
    # os.chdir(img_input)
    while True:
        print('----------------------------------------------------')
        print('把要压缩的图片放到img_input文件夹，压缩完成输出到img_out文件夹')
        menus = [
            {
                'menu': '固定大小压缩',
                'handle': start
            },
            {
                'menu': '百分比压缩',
                'handle': start
            },
        ]

        idx = utils.ask([m['menu'] for m in menus], '选择压缩方式')
        idx = str(idx)

        if idx == '0':
            val = int(input('缩放固定大小(KB),最小10：'))
            if val >= 10:
                start(_type='fixed', val=val)
                break
            else:
                continue
        elif idx == '1':
            val = int(input('缩放百分比(%)，最小10：'))
            if val >= 10:
                start(_type='ratio', val=val)
                break
            else:
                continue
        else:
            continue


if __name__ == '__main__':
    main()
