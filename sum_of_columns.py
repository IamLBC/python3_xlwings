# author:bingcheng li
# date:2021/12/01 13:58
# email:740194688@qq.com
# coding=uft-8

# 9.计算工作表sht中有数据区域最大的行数:
# #方法一：
# max_row=sht['a1048576'].end('up').row
# #方法二：
# max_row=sht.used_range.last_cell.row

# 10.计算工作表sht中有数据区域最大的列数:
# #方法一：
# max_column=sht['xfd1'].end('left').column
# 方法二：
# max_column=sht.used_range.last_cell.column

# 11.获得工作表中已经使用的单元格区域:
# #方法一：
# rng=sht.api.UsedRange
# #方法二：
# rng=sht.used_range

import os, re
import xlwings
import utils
from decimal import Decimal


def choose_file():
    files = []
    p = os.getcwd()
    dirs = os.listdir(p)

    for file in dirs:
        abp = os.path.abspath(file)
        if os.path.isfile(abp):
            name, ext = os.path.splitext(file)
            if ext == '.xlsx' or ext == '.xls':
                files.append(file)

    while True:
        idx = utils.ask(files, '选择强牛牛要统计的文件编号')

        try:
            file = files[idx]
            abp = os.path.abspath(file)
            if os.path.exists(abp):
                break
            else:
                print('err：file is not exist')
        except Exception as e:
            print(f'err：输入错误: {e}')
    return file


def input_columns():
    while True:
        val = input('输入强牛牛要计算的所有列：')
        keys = list(set(val.upper()))
        keys.sort()
        confirm = input(f'确认列：{keys}  y/n ? ')
        if bool(re.search(r'\d', val)):
            print('error: 乖乖要计算的列只能输字母。。。')
            continue
        if confirm.upper() == 'Y':
            obj = {}
            for k in keys:
                obj[k] = 0
            return obj
        else:
            continue


def get_data(wb, init_obj):
    for sheet in wb.sheets:
        # if sheet.name == 'test':
        if sheet.name[:3] != '汇总表':
            size = sheet.used_range.size
            for k in init_obj:
                col_rng = sheet.range(f'{k}1:{k}{size}')
                col = sheet.range(col_rng).value
                for value in col:
                    try:
                        init_obj[k] += Decimal(float(value))
                    except (ValueError, TypeError) as e:
                        if value is not None:
                            utils.log(f'{col_rng.get_address(True, False, external=True)} | value: {value}')
    else:
        # print(init_obj)
        return init_obj


def write_data(wb, data):
    sheet1 = wb.sheets(1)
    headers = sheet1.used_range.rows(1).value
    new_name = '汇总表'
    count = ''

    while True:
        try:
            new_sheet = wb.sheets.add(f'{new_name}')
            break
        except Exception as e:
            wb.sheets[new_name].delete()
            continue

    new_sheet.range('A1').value = headers
    rng = new_sheet.used_range
    new_data = []
    for col in rng.columns:
        col_adr = col(1, 1).get_address(False, False)
        col_key = ''.join([i for i in col_adr if not i.isdigit()])
        try:
            val = Decimal(data[col_key]).quantize(Decimal('0.00'))
            data[col_key] = str(val)
            new_data.append(str(val))
        except KeyError:
            new_data.append('')
    utils.print_json(data)
    new_sheet.range('A2').value = new_data
    wb.sheets[new_name].activate()
    # new_sheet.autofit()


def main():
    app = xlwings.App(visible=False, add_book=False)
    app.display_alerts = False
    app.screen_updating = False
    file = choose_file()
    init_obj = input_columns()
    abp = os.path.abspath(file)
    wb = app.books.open(abp)
    # wb = xlwings.Book(file)
    # wb = xlwings.Book('test.xlsx')
    data = get_data(wb, init_obj)
    write_data(wb, data)
    wb.save()
    wb.close()
    app.quit()


if __name__ == '__main__':
    main()
