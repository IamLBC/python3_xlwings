# author:bingcheng li
# date:2021/11/30 15:16
# email:740194688@qq.com
# coding=uft-8
# os.path.split(path)	把路径分割成 dirname 和 basename，返回一个元组
# os.path.abspath(file)   输出绝对路径

# print( os.path.basename('/root/runoob.txt') )   # 返回文件名
# print( os.path.dirname('/root/runoob.txt') )    # 返回目录路径
# print( os.path.split('/root/runoob.txt') )      # 分割文件名与路径
# print( os.path.join('root','test','runoob.txt') )  # 将目录和文件名合成一个路径
# runoob.txt
# /root
# ('/root', 'runoob.txt')
# root/test/runoob.txt

import utils
import sum_of_columns, scale_images


def main_menu():
    menus = [
        {
            'menu': '统计所有sheet某些列的总和',
            'handle': sum_of_columns
        },
        {
            'menu': '压缩图片',
            'handle': scale_images
        }
    ]

    idx = utils.ask([m['menu'] for m in menus], '请选择菜单')
    handle = menus[idx]['handle']
    try:
        handle.main()
    except Exception as e:
        print(f'函数：{handle}: {e}')


def start():
    while True:
        try:
            main_menu()
        except Exception as e:
            print(f'err: {e}')
        else:
            continue


if __name__ == '__main__':
    start()
